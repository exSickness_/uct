
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

LDFLAGS+=-lncurses

uct: uct.o -lncurses

.PHONY: clean debug

clean:
	-rm uct *.o

debug: CFLAGS+=-g
debug: uct
