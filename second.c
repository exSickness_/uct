#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>

// #include <curses.h>
#include <ncurses.h>


#define PORT "6667" // Port to connect to
#define TARGET_IP "74.112.200.151" // IP to connect to
/*
 * REFERENCES
 * http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/
 * http://invisible-island.net/ncurses/man/ncurses.3x.html
*/

WINDOW *create_newwin(int height, int width, int startY, int startX);


int main(int argc, char* argv[])
{
	if(argc != 1)
	{
		fprintf(stderr, "Usage: %s\n", argv[0]);
		return(2);
	}
	initscr();
	noecho();
	// curs_set(FALSE);

 	mvprintw(0, 0, "Hello, world!");
 	refresh();
	sleep(1);
	int maxY, maxX;
	(void)maxX;
	getmaxyx(stdscr,maxY,maxX);		/* get the number of rows and columns */

	WINDOW* outputWin = create_newwin(maxY-5, maxX, 0, 0);
	wmove(outputWin, 1,1);

	struct addrinfo* results, hints = {0};

	hints.ai_family = AF_UNSPEC; // Figure out the family for me
	hints.ai_socktype = SOCK_STREAM;

	int errCode = getaddrinfo(TARGET_IP, PORT, &hints, &results);
	if(errCode) {
		fprintf(stderr, "Could not get address of %s: %s\n", "74.112.200.151", gai_strerror(errCode));
		return(3);
	}

	// printf("The results show port %hd\n", ntohs(((struct sockaddr_in *)(results->ai_addr))->sin_port));

	int sd = socket(results->ai_family, results->ai_socktype, 0);
	if(sd < 0) {
		perror("Could not open socket");
		freeaddrinfo(results);
		return 4;
	}

	if(connect(sd, results->ai_addr, results->ai_addrlen) < 0) {
		perror("Could not connect to remote");
		close(sd);
		freeaddrinfo(results);
		return 4;
	}

	// Create master and temp FD sets and zero
	fd_set master;
	fd_set read_fds;
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	// Size of buffer
	const int sz = 256;

	ssize_t read_status = 0;

	// add the sd to the master set
	FD_SET(sd, &master);
	FD_SET(STDIN_FILENO, &master);

	// Update the highest FD
	int fdmax = sd;

	// Initialize variables used when dealing with FDs
	char* buf = malloc(sizeof(*buf) * sz);
	char* userInput = malloc(sizeof(*buf) * sz);

	// Main loop
	while(1)
	{
		// Copy the master FDs'
		read_fds = master;

		// Wait until recieve at least one connection
		if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
			perror("select");
			return(1);
		}

		// Loop through open FDs
		for(int i = 0; i <= fdmax; i++)
		{
			// Check FDs for anything new
			if( FD_ISSET(i, &read_fds) )
			{
				// Stdin has data
				if( i == STDIN_FILENO)
				{
					memset(userInput, '\0', 256);
					int numBytes = 0;
					if ((numBytes = read(i, userInput, sz)) > 0) {
						printf ("Sending: %s", userInput);
						// Send input to all sockets i.e. the server
						for (int idx = 0; idx <= fdmax; idx++)
						{
							// Send to proper socket
							if (FD_ISSET (idx, &master) && idx != STDIN_FILENO) {
								if (send (idx, userInput, numBytes, 0) < 0) {
									fprintf(stderr, "Error sending message!\n");
								}
							}
						}
					}
					else {
						fprintf(stderr, "Error getting stdin!\n");
					}
				}
				// Server sent something - let's get it!
				else if(i == sd)
				{
					int recvLen = 0;
					memset(buf, '\0', 256);
					errno = read_status = 0;
					while((read_status = recv(sd, buf, sz-1, MSG_DONTWAIT)) == 255)
					{
						recvLen = 0;
						buf[read_status] = '\0';
						for( ; recvLen < read_status; recvLen++) {
 							mvwaddch(outputWin, 1,1+recvLen, buf[recvLen]);
				 			wrefresh(outputWin);
						}
					}
					recvLen = 0;
					for( ; recvLen < read_status; recvLen++) {
 						mvwaddch(outputWin, 1,1+recvLen, buf[recvLen]);
					 	wrefresh(outputWin);
					}
				}
			}
		}
	}

	endwin();
	free(buf);
	free(userInput);
}


WINDOW *create_newwin(int height, int width, int startY, int startX)
{	WINDOW *local_win;

	local_win = newwin(height, width, startY, startX);
	box(local_win, 0 , 0);		/* 0, 0 gives default characters
					 * for the vertical and horizontal
					 * lines			*/
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

