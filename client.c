#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>


int main(int argc, char* argv[])
{
	if(argc != 1)
	{
		fprintf(stderr, "Usage: %s <hostname>\n", argv[0]);
		return(2);
	}

	struct addrinfo* results, hints = {0};

	hints.ai_family = AF_UNSPEC; // Figure out the family for me
	hints.ai_socktype = SOCK_STREAM;

	int errCode = getaddrinfo("74.112.200.151", "6667", &hints, &results);
	if(errCode) {
		fprintf(stderr, "Could not get address of %s: %s\n", "74.112.200.151", gai_strerror(errCode));
		return(3);
	}


	printf("The results show port %hd\n", ntohs(((struct sockaddr_in *)(results->ai_addr))->sin_port));

	int sd = socket(results->ai_family, results->ai_socktype, 0);
	if(sd < 0) {
		perror("Could not open socket");
		freeaddrinfo(results);
		return 4;
	}

	if(connect(sd, results->ai_addr, results->ai_addrlen) < 0) {
		perror("Could not connect to remote");
		close(sd);
		freeaddrinfo(results);
		return 4;
	}

	ssize_t read_status = 0;
	char buf[256] = {'\0'};
	char choice[256] = {'\0'};

	while(1)
	{
		sleep(1);
		printf("Waiting for reply.\n");
		memset(buf, '\0', 256);
		errno = read_status = 0;
		while((read_status = recv(sd, buf, sizeof(buf)-1, MSG_DONTWAIT)) == 255)
		{
			buf[read_status] = '\0';

			printf("%s", buf);
			memset(buf, '\0', 256);
		}
		printf("%s\n", buf);

		if(errno == EAGAIN ||errno == EWOULDBLOCK)
		{
			memset(choice, '\0', 256);
			printf("Enter something:\n");
			fgets(choice, sizeof(choice), stdin);

			if(write(sd, choice, strlen(choice)) < 0) {
				perror("Could not write to remote");
				close(sd);
				freeaddrinfo(results);
				return 5;
			}
		}
		else if(read_status < 0) {
			perror("Could not read from remote");
			close(sd);
			freeaddrinfo(results);
			return 6;
		}
	}


	close(sd);
	freeaddrinfo(results);
	return(0);
}