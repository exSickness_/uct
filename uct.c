#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>

// #include <curses.h>
#include <ncurses.h>
#include <locale.h>


#define PORT "6667" // Port to connect to
#define TARGET_IP "74.112.200.151" // IP to connect to
/*
 * REFERENCES
 * http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/
 * http://invisible-island.net/ncurses/man/ncurses.3x.html
 * https://www.mkssoftware.com/docs/man3/curs_inopts.3.asp
*/

WINDOW *create_newwin(int height, int width, int startY, int startX);
int checkSocket(char* buf, WINDOW* outputWin, int sd, int sz, int scrollOn, int* curPos);
void addInputToOutput(char* buf, WINDOW* outputWin, int scrollOn, int* curPos);


int main(int argc, char* argv[])
{
	if(argc != 1)
	{
		fprintf(stderr, "Usage: %s\n", argv[0]);
		return(2);
	}

	struct addrinfo* results, hints = {0};

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	int errCode = getaddrinfo(TARGET_IP, PORT, &hints, &results);
	if(errCode) {
		fprintf(stderr, "Could not get address of %s: %s\n", "74.112.200.151", gai_strerror(errCode));
		return(3);
	}

	int sd = socket(results->ai_family, results->ai_socktype, 0);
	if(sd < 0) {
		perror("Could not open socket");
		freeaddrinfo(results);
		return 4;
	}

	if(connect(sd, results->ai_addr, results->ai_addrlen) < 0) {
		perror("Could not connect to remote");
		close(sd);
		freeaddrinfo(results);
		return 4;
	}

	// Size of buffer
	const int sz = 256;

	// Initialize variables used for data
	char* buf = malloc(sizeof(*buf) * sz);
	char* userInput = malloc(sizeof(*userInput) * sz);
	char* sanString = malloc(sizeof(*sanString) * sz);
	char* curChan = malloc(sizeof(*curChan) * sz);
	char* onlyMsg = malloc(sizeof(*curChan) * sz);
	memset(buf, '\0', sz);
	memset(userInput, '\0', sz);
	memset(sanString, '\0', sz);
	memset(curChan, '\0', sz);
	memset(onlyMsg, '\0', sz);
	int userInputLen = 0;

	char* tempUser = malloc(sizeof(*tempUser) * 16);
	printf("Enter your username:");
	fgets(tempUser, 15, stdin);
	int tempLen = strlen(tempUser);
	tempUser[tempLen-1] = ' ';

	char* nick = malloc(sizeof(*nick) * 16);
	printf("Enter your nick:");
	fgets(nick, 15, stdin);

	printf("Enter your real name:");
	fgets(buf, sz, stdin);

	int curSpot = 0;
	strncpy(userInput, "user ", 5);
	curSpot = strlen(userInput);

	strncpy(&userInput[curSpot], tempUser, strlen(tempUser));
	curSpot = strlen(userInput);

	strncpy(&userInput[curSpot], nick, strlen(nick)-1);
	curSpot = strlen(userInput);

	strncpy(&userInput[curSpot], " localhost ", 11);
	curSpot = strlen(userInput);

	strncpy(&userInput[curSpot], buf, strlen(buf));

	if (send (sd, userInput, strlen(userInput), 0) < 0) {
		fprintf(stderr, "Error sending message!\n");
	}
	memset(userInput, '\0', sz);
	strncpy(userInput, "nick ", 5);
	strncpy(&userInput[5], nick, strlen(nick));

	if (send (sd, userInput, strlen(userInput), 0) < 0) {
		fprintf(stderr, "Error sending message!\n");
	}

	memset(userInput, '\0', sz);

	free(nick);
	free(tempUser);

	// Set up curses variables
	setlocale(LC_ALL, "");
	initscr();
	noecho();
	cbreak();
	curs_set(FALSE);

	int maxY, maxX;
	getmaxyx(stdscr,maxY,maxX);

	// Create curses windows
	WINDOW* outputWin = create_newwin(maxY-5, maxX, 0, 0);
	if(!outputWin) {
		fprintf(stderr, "%s\n", "Error creating output window");
		return(-1);
	}
	wmove(outputWin, 1,1);
	scrollok(outputWin, TRUE);

	WINDOW* inputWin = create_newwin(3, maxX, maxY-3, 0);
	if(!inputWin) {
		fprintf(stderr, "%s\n", "Error creating input window");
		return(-1);
	}
	wmove(inputWin, 1,1);
	nodelay(inputWin, TRUE);

	// Curses variables used for scrolling
	int curPos = 1;
	int outputWinX, outputWinY;
	getmaxyx(outputWin,outputWinY,outputWinX);
	(void)outputWinX;
	int scrollOn = outputWinY-3;
	wsetscrreg(outputWin,1,scrollOn+1);

	int c = -1;
	int retVal = -1;
	// Main loop
	while(1)
	{
		retVal = checkSocket(buf, outputWin, sd, sz, scrollOn, &curPos);
		if(retVal == -1) {
			break;
		}
		c = wgetch(inputWin);

		if(c == -1) {
			continue;
		}

		if(c == '\n')
		{
			if(strncmp("/quit", sanString, 5) == 0) {
				break;
			}
			else if(strncmp("/chan", sanString, 5) == 0) {
				userInput[userInputLen] = c;
				userInputLen++;
				memset(curChan, '\0', sz);
				strncpy(curChan, &sanString[6], userInputLen-5);
				printf("%s", curChan);
			}
			else if(userInput[0] == '/') {
				userInput[userInputLen] = c;
				userInputLen++;
				if (send (sd, &userInput[1], userInputLen-1, 0) < 0) {
					fprintf(stderr, "Error sending message!\n");
				}
			}
			else
			{
				userInput[userInputLen] = c;
				userInputLen++;

				memset(onlyMsg, '\0', sz);
				strncpy(onlyMsg, "privmsg ", 8);
				curChan[strlen(curChan)] = ' ';
				strncpy(&onlyMsg[8], curChan, sz -10);
				int lenOfChan = strlen(curChan);
				int lenOfMsg = strlen(onlyMsg);
				strncpy(&onlyMsg[lenOfMsg], userInput, sz-lenOfChan - 8);

				if (send (sd, onlyMsg, strlen(onlyMsg), 0) < 0) {
					fprintf(stderr, "Error sending message!\n");
				}
			}

			addInputToOutput(userInput, outputWin, scrollOn, &curPos);

			memset(userInput, '\0', sz);
			memset(sanString, '\0', sz);
			userInputLen = 0;
			wmove(inputWin, 1,0);
			werase(inputWin);
		}
		else if( (c == KEY_BACKSPACE) || (c == 127) )
		{
			if (userInputLen > 0)
			{
				wmove(inputWin, 1,userInputLen);
				wclrtoeol(inputWin);
				userInput[userInputLen] = '\0';
				userInputLen--;
			}
		}
		else
		{
			userInput[userInputLen] = c;
			wmove(inputWin, 1,userInputLen);
			mvwaddch(inputWin, 1,1+userInputLen, userInput[userInputLen]);
			c = tolower(c);
			sanString[userInputLen] = c;
			userInputLen++;
		}

		box(inputWin, 0,0);
 		wrefresh(inputWin);
	}

	endwin();
	free(onlyMsg);
	free(curChan);
	free(sanString);
	free(userInput);
	free(buf);
	freeaddrinfo(results);
	return(0);
}


void addInputToOutput(char* userInput, WINDOW* outputWin, int scrollOn, int* curPos)
{
	int xOffset = 1;
	mvwaddch(outputWin, *curPos, xOffset, '>');
	xOffset++;

	size_t length = strlen(userInput);
	for( size_t i = 0; i < length-1; i++, xOffset++){
		mvwaddch(outputWin, *curPos, xOffset, userInput[i]);
	}

	if(*curPos+1 > scrollOn) {
		wscrl(outputWin, 1);
	}
	else {
		*curPos+=1;
	}

	box(outputWin, 0,0);
	wrefresh(outputWin);
}

int checkSocket(char* buf, WINDOW* outputWin, int sd, int sz, int scrollOn, int* curPos)
{
	ssize_t read_status = 0;
	int recvLen = 0;
	int xOffset = 0;
	memset(buf, '\0', sz);
	errno = read_status = 0;

	// Loop to read out buffer when it's full (256B)
	while((read_status = recv(sd, buf, sz-1, MSG_DONTWAIT)) == 255)
	{
		if(strstr(buf, "PING")) {
			if (send (sd, "PONG\n", 5, 0) < 0) {
				fprintf(stderr, "Error sending message!\n");
			}
		}
		recvLen = 0;
		buf[read_status] = '\0';
		for( ; recvLen < read_status; recvLen++, xOffset++) {
			if(buf[recvLen] == '\n') {
				xOffset = -1;
				if(*curPos > scrollOn) {
					wscrl(outputWin, 1);
				}
				else {
					*curPos += 1;
				}
			}
			else {
				mvwaddch(outputWin, *curPos,1+xOffset, buf[recvLen]);
			}
		}

	}
	if(strstr(buf, "PING")) {
		if (send (sd, "PONG\n", 5, 0) < 0) {
			fprintf(stderr, "Error sending message!\n");
		}
	}
	if(read_status == 0) {
		return(-1);
	}

	// Read out data that's less than the buffer size (256)
	recvLen = 0;
	for( ; recvLen < read_status; recvLen++, xOffset++) {
		if(buf[recvLen] == '\n') {
			xOffset = -1;
			if(*curPos > scrollOn) {
				wscrl(outputWin, 1);
			}
			else {
				*curPos += 1;
			}
		}
		else {
			mvwaddch(outputWin, *curPos,1+xOffset, buf[recvLen]);
		}
	}

	box(outputWin, 0,0);
 	wrefresh(outputWin);
 	return(0);
}


WINDOW *create_newwin(int height, int width, int startY, int startX)
{	WINDOW *local_win;

	local_win = newwin(height, width, startY, startX);
	box(local_win, 0 , 0);		/* 0, 0 gives default characters
					 * for the vertical and horizontal
					 * lines			*/
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

